﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModPacker
{
    /// <summary>
    /// Interaction logic for ModPackageView.xaml
    /// </summary>
    public partial class ModPackageView : UserControl
    {
        public static RoutedCommand AddMapFileCommand = new RoutedCommand();
        public static RoutedCommand RemoveMapFileCommand = new RoutedCommand();
        public static RoutedCommand AddCampaignFileCommand = new RoutedCommand();
        public static RoutedCommand RemoveCampaignFileCommand = new RoutedCommand();

        public ModPackageView()
        {
            InitializeComponent();
        }

        private void AddMapFileCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void AddMapFileCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var viewModel = this.DataContext as ModPackageViewModel;

            var ofd = new OpenFileDialog();
            ofd.Title = "Open";
            ofd.DefaultExt = "Map files (*.map)";
            ofd.CheckFileExists = true;
            if (ofd.ShowDialog() == false)
                return;

            var fileInfo = new FileInfo(ofd.FileName);
            if (fileInfo.Extension == ".map")
            {
                viewModel.AddMapFile(fileInfo);
            }
        }

        private void AddCampaignFileCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var viewModel = this.DataContext as ModPackageViewModel;
            e.CanExecute = viewModel?.Campaign == null;
            e.Handled = true;
        }

        private void AddCampaignFileCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var viewModel = this.DataContext as ModPackageViewModel;

            var ofd = new OpenFileDialog();
            ofd.Title = "Open";
            ofd.FileName = "halo3.campaign";
            ofd.DefaultExt = "Campaign files (*.campaign)";
            ofd.CheckFileExists = true;
            if (ofd.ShowDialog() == false)
                return;

            var fileInfo = new FileInfo(ofd.FileName);
            if (fileInfo.Extension == ".campaign")
            {
                viewModel.AddCampaignFile(fileInfo);
            }
        }

        private void RemoveMapFileCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var viewModel = this.DataContext as ModPackageViewModel;
            e.CanExecute = viewModel?.SelectedMapFileEntry != null;
            e.Handled = true;
        }

        private void RemoveMapFileCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var viewModel = this.DataContext as ModPackageViewModel;
            viewModel.RemoveSelectedMapFile();
        }

        private void RemoveCampaignFileCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var viewModel = this.DataContext as ModPackageViewModel;
            e.CanExecute = viewModel?.Campaign != null;
            e.Handled = true;
        }

        private void RemoveCampaignFileCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var viewModel = this.DataContext as ModPackageViewModel;
            viewModel.RemoveCampaignFile();
        }
    }
}
