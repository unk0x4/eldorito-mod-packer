﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TagTool.Cache;
using TagTool.IO;

namespace ModPacker
{
    class ModPackageViewModel : ViewModelBase, ISavableFile
    {
        private FileInfo PackageFile;
        private ModPackageExtended Package;

        public string Name { get; set; }
        public string Hash { get; set; }

        public ModPackageMetadataViewModel Metadata { get; set; }
        public TagListViewModel TagList { get; set; }

        private CampaignFileViewModel _campaign;
        public CampaignFileViewModel Campaign
        {
            get { return _campaign; }
            set
            {
                _campaign = value;
                NotifyPropertyChanged(nameof(Campaign));
            }
        }

        private ModPackageMapFilesViewModel _mapFiles;
        public ModPackageMapFilesViewModel MapFiles
        {
            get { return _mapFiles; }
            set
            {
                _mapFiles = value;
                NotifyPropertyChanged(nameof(MapFiles));
            }
        }

        private MapFileEntryViewModel _selectedMapFileEntry;
        public MapFileEntryViewModel SelectedMapFileEntry
        {
            get { return _selectedMapFileEntry; }
            set
            {
                _selectedMapFileEntry = value;
                NotifyPropertyChanged(nameof(SelectedMapFileEntry));
            }
        }

        public ModPackageViewModel(FileInfo file, ModPackageExtended package)
        {
            PackageFile = file;
            Package = package;

            Hash = BitConverter.ToString(package.Header.SHA1).Replace("-", "").ToLower();
            Metadata = new ModPackageMetadataViewModel(package.Metadata);
            TagList = new TagListViewModel(GetTagEntries());
            MapFiles = new ModPackageMapFilesViewModel(GetMapFileEntries());
            if (package.CampaignFileStream.Length > 0)
                Campaign = new CampaignFileViewModel();
        }

        public void AddMapFile(FileInfo fileInfo)
        {
            using (var stream = fileInfo.OpenRead())
            {
                var mapFile = new MapFile(new EndianReader(stream, EndianFormat.LittleEndian));
                stream.Position = 0;
                var packageMapFileStream = new MemoryStream();
                stream.CopyTo(packageMapFileStream);
                packageMapFileStream.Position = 0;
                Package.MapFileStreams.Add(packageMapFileStream);
                MapFiles = new ModPackageMapFilesViewModel(GetMapFileEntries());
            }
        }

        public void RemoveSelectedMapFile()
        {
            var index = MapFiles.Entries.IndexOf(SelectedMapFileEntry);
            if (index == -1)
                throw new NotSupportedException();

            Package.MapFileStreams.RemoveAt(index);
            MapFiles.Entries.RemoveAt(index);
        }

        public void AddCampaignFile(FileInfo fileInfo)
        {
            if (Package.CampaignFileStream.Length > 0)
                throw new NotSupportedException();

            using (var stream = fileInfo.OpenRead())
            {
                Package.CampaignFileStream.Position = 0;
                stream.CopyTo(Package.CampaignFileStream);

                Campaign = new CampaignFileViewModel();
            }
        }

        public void RemoveCampaignFile()
        {
            if (Package.CampaignFileStream.Length < 1)
                throw new NotSupportedException();

            Package.CampaignFileStream = new MemoryStream();
            Campaign = null;
        }

        private bool SaveInternal(FileInfo fileInfo)
        {
            Package.Metadata.Author = Metadata.Author;
            Package.Metadata.Name = Metadata.Name;
            Package.Metadata.Description = Metadata.Description;
            Package.Save(fileInfo);
            return true;
        }

        public bool CanBeSaved()
        {
            return true;
        }

        public bool Save()
        {
            return SaveInternal(PackageFile);
        }

        public bool SaveAs(FileInfo fileInfo)
        {
            var saved = SaveInternal(fileInfo);
            if (saved)
                PackageFile = fileInfo;
            return saved;
        }

        private IEnumerable<MapFileEntryViewModel> GetMapFileEntries()
        {
            foreach (var stream in Package.MapFileStreams)
            {
                var mapFile = new MapFile(new EndianReader(stream, EndianFormat.LittleEndian));
                yield return new MapFileEntryViewModel()
                {
                    Name = mapFile.BlfInformation.MapNameInternal,
                    MapId = mapFile.BlfInformation.MapId
                };
            }
        }

        private IEnumerable<TagEntryViewModel> GetTagEntries()
        {
            return
                from item in Package.Tags.Index
                where item != null
                select new TagEntryViewModel()
                {
                    GroupTag = item.Group.Tag.ToString(),
                    Instance = item,
                    Name = item.Name,
                    Version = CacheVersion.HaloOnline106708
                };
        }
    }
}
