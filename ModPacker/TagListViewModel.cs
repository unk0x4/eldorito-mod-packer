﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using TagTool.Cache;

namespace ModPacker
{
    class TagListViewModel
    {
        public ObservableCollection<TagEntryViewModel> Entries { get; set; }

        public TagListViewModel(IEnumerable<TagEntryViewModel> entries)
        {
            Entries = new ObservableCollection<TagEntryViewModel>(entries);
        }
    }

    class TagEntryViewModel
    {
        public string GroupTag { get; set; }
        public string Name { get; set; }
        public CacheVersion Version { get; set; }
        public object Instance { get; set; }
    }

    class TagGroupViewModel
    {
        public string GroupTag { get; set; }
        public string Description { get; set; }
        public List<TagEntryViewModel> Entries { get; set; }

        public TagGroupViewModel(string groupTag, IEnumerable<TagEntryViewModel> entries)
        {
            GroupTag = groupTag;
            Entries = new List<TagEntryViewModel>(entries);
        }
    }
}
