﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModPacker
{
    class ModPackageMapFilesViewModel
    {
        public ObservableCollection<MapFileEntryViewModel> Entries { get; set; }

        public ModPackageMapFilesViewModel(IEnumerable<MapFileEntryViewModel> entries)
        {
            Entries = new ObservableCollection<MapFileEntryViewModel>(entries);
        }
    }

    class MapFileEntryViewModel
    {
        public string Name { get; set; }
        public int MapId { get; set; }
    }
}
