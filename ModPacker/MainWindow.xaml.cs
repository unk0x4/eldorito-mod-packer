﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TagTool.Cache;

namespace ModPacker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static RoutedCommand OpenCommand { get; set; } = new RoutedCommand();
        public static RoutedCommand SaveCommand { get; set; } = new RoutedCommand();
        public static RoutedCommand SaveAsCommand { get; set; } = new RoutedCommand();
        public static RoutedCommand ExitCommand { get; set; } = new RoutedCommand();

        public ObservableCollection<object> Tabs { get; set; } = new ObservableCollection<object>();
        public object ActiveTab { get; set; }

        private static string PackageFileFilter = "Package files (*.pak)|*.pak";

        public MainWindow()
        {
            this.DataContext = this;
            InitializeComponent();
        }

        private void OpenCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Title = "Open";
            ofd.Filter = PackageFileFilter;
            ofd.CheckFileExists = true;
            if (ofd.ShowDialog() == false)
                return;
            
            var fileInfo = new FileInfo(ofd.FileName);
            if (fileInfo.Extension == ".pak")
            {
                var modpackage = new TagTool.Cache.ModPackageExtended(fileInfo);
                var viewModel = new ModPackageViewModel(fileInfo, modpackage) { Name = fileInfo.Name };
                Tabs.Add(viewModel);
            }
        }

        private void OpenCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void SaveCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var savable = ActiveTab as ISavableFile;
            if (savable == null)
                throw new NotSupportedException();

            savable.Save();
        }

        private void SaveCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var savable = ActiveTab as ISavableFile;
            e.CanExecute = savable?.CanBeSaved() ?? false;
            e.Handled = true;
        }

        private void SaveAsCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var savable = ActiveTab as ISavableFile;
            if (savable == null)
                throw new NotSupportedException();

            var sfd = new SaveFileDialog();
            sfd.Title = "Save";
            sfd.Filter = PackageFileFilter;
            if (sfd.ShowDialog() == false)
                return;

            var fileInfo = new FileInfo(sfd.FileName);
            if (fileInfo.Extension == ".pak")
            {
                savable.SaveAs(fileInfo);
            }
        }

        private void SaveAsCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            SaveCommand_CanExecute(sender, e);
        }

        private void ExitCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.Current.Shutdown();
        }
    }
}
