﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModPacker
{
    interface ISavableFile
    {
        bool CanBeSaved();
        bool Save();
        bool SaveAs(FileInfo fileInfo);
    }
}
