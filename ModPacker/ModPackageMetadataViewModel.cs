﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TagTool.Cache;

namespace ModPacker
{
    class ModPackageMetadataViewModel
    {
        public string Name { get; set; }

        public string Author { get; set; }

        public string Description { get; set; }

        public ModPackageMetadataViewModel(ModPackageMetadata metadata)
        {
            Name = metadata.Name;
            Author = metadata.Author;
            Description = metadata.Description;
        }
    }
}
